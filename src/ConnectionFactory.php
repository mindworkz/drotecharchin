<?php

namespace Archin\ExactOnline;

use Dotenv\Dotenv;
use Picqer\Financials\Exact\Connection;

class ConnectionFactory
{
    /**
     * Create a new connection.
     *
     * @return Connection
     */
    public static function create(): Connection
    {
        self::loadConfiguration();

        // Create connection
        $connection = new Connection();

        $connection->setRedirectUrl($_ENV['REDIRECT_URL']);
        $connection->setExactClientId($_ENV['EXACT_CLIENT_ID']);
        $connection->setExactClientSecret($_ENV['EXACT_CLIENT_SECRET']);
        $connection->setTokenUpdateCallback([ConnectionFactory::class, 'updateTokens']);

        // Load settings
        $settings = SettingsStorage::load();

        if (isset($settings->accessToken)) {
            $connection->setAccessToken($settings->accessToken);
        }

        if (isset($settings->refreshToken)) {
            $connection->setRefreshToken($settings->refreshToken);
        }

        return $connection;
    }

    /**
     * Load Dotenv configuration.
     */
    public static function loadConfiguration(): void
    {
        $configuration = Dotenv::createImmutable(__DIR__ . '/../');
        $configuration->load();
    }

    /**
     * Update tokens and store in storage.
     *
     * @param Connection $connection
     */
    public static function updateTokens(Connection $connection): void
    {
        $settings = SettingsStorage::load();

        $settings->accessToken = $connection->getAccessToken();
        $settings->refreshToken = $connection->getRefreshToken();

        SettingsStorage::save($settings);
    }
}
