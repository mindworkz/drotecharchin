<?php

namespace Archin\ExactOnline;

class SettingsStorage
{
    /**
     * Cipher for encrypting/decrypting.
     *
     * @var string
     */
    private static $cipher = 'AES-128-CBC';

    /**
     * Load en decrypt settings from storage.
     *
     * @return Settings
     */
    public static function load(): Settings
    {
        $settings = new Settings();

        if (!file_exists(__DIR__ . '/../storage/settings')) {
            return $settings;
        }

        $data = file_get_contents(__DIR__ . '/../storage/settings');
        $data = self::decryptData($data);

        $storage = json_decode($data);

        if (isset($storage->accessToken)) {
            $settings->accessToken = $storage->accessToken;
        }

        if (isset($storage->refreshToken)) {
            $settings->refreshToken = $storage->refreshToken;
        }

        return $settings;
    }

    /**
     * Encrypt and save settings to storage.
     *
     * @param Settings $settings
     */
    public static function save(Settings $settings): void
    {
        $data = json_encode($settings);
        $data = self::encryptData($data);

        file_put_contents(__DIR__ . '/../storage/settings', $data);
    }

    /**
     * Decrypt the given data.
     * Note: using an empty IV is known less secure.
     *
     * @param mixed $data
     * @return string
     */
    private static function decryptData($data): string
    {
        return @openssl_decrypt($data, self::$cipher, $_ENV['ENCRYPTION_KEY']);
    }

    /**
     * Encrypt the given data.
     * Note: using an empty IV is known less secure.
     *
     * @param mixed $data
     * @return string
     */
    private static function encryptData($data): string
    {
        return @openssl_encrypt($data, self::$cipher, $_ENV['ENCRYPTION_KEY']);
    }
}
