<?php

namespace Archin\ExactOnline;

class Language
{
    const DE = 'DE';
    const EN = 'EN';
    const FR = 'FR';
    const NL = 'NL';

    /**
     * Get all available languages.
     *
     * @return array
     */
    public static function all(): array
    {
        return [
            self::NL,
            self::EN,
            self::DE,
            self::FR,
        ];
    }
}
