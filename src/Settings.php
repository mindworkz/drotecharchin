<?php

namespace Archin\ExactOnline;

class Settings
{
    /**
     * @var string|null
     */
    public $accessToken = null;

    /**
     * @var string|null
     */
    public $refreshToken = null;
}
