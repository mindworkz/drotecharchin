<?php

namespace Archin\ExactOnline\Models;

use Picqer\Financials\Exact\ApiException;
use Picqer\Financials\Exact\Query\Findable as BaseFindable;

trait Findable
{
    use BaseFindable;

    /**
     * Get all results
     *
     * @param string $filter
     * @param string $expand
     * @param string $select
     * @param array $headers
     * @return array
     * @throws ApiException
     */
    public function getAll($select = '', $filter = '', $expand = '', array $headers = [])
    {
        $request = [];

        if (!is_null($select)) {
            $request['$select'] = $select;
        }

        if (!is_null($expand)) {
            $request['$filter'] = $filter;
        }

        if (!is_null($expand)) {
            $request['$expand'] = $expand;
        }

        $result = $this->connection()->get($this->url(), $request, $headers);

        return $this->collectionAttributesFromResult($result, $headers);
    }

    /**
     * Get list of sanitised attributes.
     *
     * @param $result
     * @param array $headers
     * @return array
     */
    public function collectionAttributesFromResult($result, array $headers = [])
    {
        $items = $this->collectionFromResult($result, $headers);
        $serializedItems = [];

        foreach ($items as $item) {
            // Serialize item and trim all values
            $serialized = array_map(function ($value) {
                return $value !== null ? trim($value) : null;
            }, $item->jsonSerialize());

            $serializedItems[] = $serialized;
        }

        return $serializedItems;
    }

    /**
     * NOTE: Overriden to add headers.
     * TODO: Remove after this PR has been merged: https://github.com/picqer/exact-php-client/pull/457
     *
     * @param array $result
     * @param array $headers
     * @return array
     * @throws ApiException
     */
    public function collectionFromResult($result, array $headers = [])
    {
        // If we have one result which is not an assoc array, make it the first element of an array for the
        // collectionFromResult function so we always return a collection from filter
        if ((bool)count(array_filter(array_keys($result), 'is_string'))) {
            $result = [$result];
        }

        while ($this->connection()->nextUrl !== null) {
            $nextResult = $this->connection()->get($this->connection()->nextUrl, [], $headers);

            // If we have one result which is not an assoc array, make it the first element of an array for the array_merge function
            if ((bool)count(array_filter(array_keys($nextResult), 'is_string'))) {
                $nextResult = [$nextResult];
            }

            $result = array_merge($result, $nextResult);
        }
        $collection = [];
        foreach ($result as $r) {
            $collection[] = new self($this->connection(), $r);
        }

        return $collection;
    }
}
