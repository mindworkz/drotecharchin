<?php

namespace Archin\ExactOnline\Models;

use Archin\ExactOnline\Language;
use Picqer\Financials\Exact\ApiException;

class Item extends \Picqer\Financials\Exact\Item
{
    use Findable;

    /**
     * Get (translated) items.
     *
     * @param string $language
     * @return array
     * @throws ApiException
     */
    public function getItems($language = Language::NL): array
    {
        return $this->getAll(
            'Description, ExtraDescription, ItemGroup, Class_01, Class_02, Code, PictureUrl',
            'IsWebshopItem eq 1',
            '',
            ['CustomDescriptionLanguage' => $language]
        );
    }

    /**
     * Get stocks of items.
     *
     * @return array
     * @throws ApiException
     */
    public function getStocks(): array
    {
        return $this->getAll('Code, Stock');
    }
}
