<?php

namespace Archin\ExactOnline\Models;

use Picqer\Financials\Exact\ApiException;
use Picqer\Financials\Exact\SalesItemPrice as SalesItemPrice;

class SalesPrices extends SalesItemPrice
{
    use Findable;

    /**
     * Get sales prices
     *
     * @return array
     * @throws ApiException
     */
    public function getSalesPrices()
    {
        return $this->getAll('ItemCode, Price, Quantity, Account');
    }
}
