<?php

namespace Archin\ExactOnline\Models;

use Picqer\Financials\Exact\ApiException;

class Account extends \Picqer\Financials\Exact\Account
{
    use Findable;

    /**
     * Get sales accounts.
     *
     * @return array
     * @throws ApiException
     */
    public function getSalesAccounts(): array
    {
        return $this->getAll(
            'ID, Code, Name, AddressLine1, AddressLine2, AddressLine3, Postcode, City, Email, Country, MainContact',
            'IsSales eq true'
        );
    }
}
