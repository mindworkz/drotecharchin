<?php

namespace Archin\ExactOnline\Models;

use Picqer\Financials\Exact\ApiException;
use Picqer\Financials\Exact\Connection;

class Category
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * Create new class to load categories.
     * A category is an item group or an item assortment property.
     *
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Get categories based on item groups and item assortment properties.
     *
     * @return array
     * @throws ApiException
     */
    public function getCategories(): array
    {
        $categories = [];

        // Get item groups
        $itemGroup = new ItemGroup($this->connection);
        $itemGroups = $itemGroup->getItemGroups();

        foreach ($itemGroups as $itemGroup) {
            $itemGroup['Type'] = 'ItemGroup';

            $categories[] = $itemGroup;
        }

        // Get item assortment properties
        $itemAssortmentProperty = new ItemAssortmentProperty($this->connection);
        $properties = $itemAssortmentProperty->getProperties();

        foreach ($properties as $property) {
            $property['Type'] = (int)$property['ItemAssortmentCode'] === Product::SUB_CATEGORY
                ? 'SubCategory'
                : 'DetailCategory';

            unset($property['ItemAssortmentCode']);

            $categories[] = $property;
        }

        return $categories;
    }
}
