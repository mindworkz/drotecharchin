<?php

namespace Archin\ExactOnline\Models;

use GuzzleHttp\Client;
use Picqer\Financials\Exact\Connection;

class Picture
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * Create new class to load products.
     * A product consists of data of multiple calls for items (for languages).
     *
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function download($pictureUrl)
    {
        $client = new Client();

        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Prefer' => 'return=representation',
            'Authorization' => 'Bearer ' . $this->connection->getAccessToken(),
        ];

        $res = $client->get($pictureUrl, [
            'headers' => $headers,
        ]);

        return $res->getBody();
    }
}
