<?php

namespace Archin\ExactOnline\Models;

use Archin\ExactOnline\Language;
use Picqer\Financials\Exact\ApiException;
use Picqer\Financials\Exact\Connection;

class Product
{
    /**
     * Code of item assortment 'Sub category'.
     */
    const SUB_CATEGORY = 1;

    /**
     * Code of item assortment 'Detail category'.
     */
    const DETAIL_CATEGORY = 2;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * Fields to use with translations.
     *
     * @var array
     */
    private $translatedFields = [
        'Description',
        'ExtraDescription',
    ];

    /**
     * Create new class to load products.
     * A product consists of data of multiple calls for items (for languages).
     *
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Get products with all required languages.
     *
     * @param array|null $languages Languages to add; when null use all default languages.
     * @return array
     * @throws ApiException
     */
    public function getProducts(array $languages = null): array
    {
        $itemModel = new Item($this->connection);

        if ($languages === null) {
            $languages = Language::all();
        }

        $products = [];

        foreach ($languages as $language) {
            $items = $itemModel->getItems($language);

            foreach ($items as $item) {
                if (!isset($products[$item['Code']])) {
                    // Rename fields
                    $item['SubCategory'] = $item['Class_0' . self::SUB_CATEGORY];
                    $item['DetailCategory'] = $item['Class_0' . self::DETAIL_CATEGORY];

                    unset($item['Class_0' . self::SUB_CATEGORY], $item['Class_0' . self::DETAIL_CATEGORY]);

                    // Add product
                    $products[$item['Code']] = $item;
                }

                // Set translated fields
                foreach ($this->translatedFields as $field) {
                    $products[$item['Code']][$field . $language] = $item[$field];
                }
            }

        }

        return array_values($products);
    }

    /**
     * Get stocks of products.
     *
     * @return array
     * @throws ApiException
     */
    public function getStocks(): array
    {
        return (new Item($this->connection))
            ->getStocks();
    }
}
