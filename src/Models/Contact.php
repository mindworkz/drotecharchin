<?php

namespace Archin\ExactOnline\Models;

use Picqer\Financials\Exact\ApiException;

class Contact extends \Picqer\Financials\Exact\Contact
{
    use Findable;

    /**
     * Get sales contacts.
     *
     * @return array
     * @throws ApiException
     */
    public function getSalesContacts(): array
    {
        return $this->getAll('ID, FullName', 'AccountIsCustomer eq true and IsMainContact eq true');
    }
}
