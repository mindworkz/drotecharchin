<?php

namespace Archin\ExactOnline\Models;

use Picqer\Financials\Exact\ApiException;

class ItemGroup extends \Picqer\Financials\Exact\ItemGroup
{
    use Findable;

    /**
     * Get item groups.
     *
     * @return array
     * @throws ApiException
     */
    public function getItemGroups()
    {
        return $this->getAll('ID, Description');
    }
}
