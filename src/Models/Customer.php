<?php

namespace Archin\ExactOnline\Models;

use Picqer\Financials\Exact\ApiException;
use Picqer\Financials\Exact\Connection;

class Customer
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * Create new class to load customers.
     * A customer consists of data of a sales account and a contact.
     *
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Get customers based on sales accounts and main contacts.
     *
     * @return array
     * @throws ApiException
     */
    public function getCustomers(): array
    {
        $account = new Account($this->connection);

        $accounts = $account->getSalesAccounts();
        $contacts = $this->getMappedContacts();

        // Loop accounts to add main contact's full name
        foreach ($accounts as &$account) {
            $contactId = $account['MainContact'];

            if ($contactId === null || !isset($contacts[$contactId])) {
                // No main contact or main contact isn't known
                continue;
            }

            $account['MainContactFullName'] = $contacts[$contactId]['FullName'];
        }

        return $accounts;
    }

    /**
     * Get contacts mapped by ID.
     *
     * @return array
     * @throws ApiException
     */
    private function getMappedContacts(): array
    {
        $contact = new Contact($this->connection);
        $contacts = $contact->getSalesContacts();

        $mappedContacts = [];

        foreach ($contacts as $contact) {
            $mappedContacts[$contact['ID']] = $contact;
        }

        return $mappedContacts;
    }
}
