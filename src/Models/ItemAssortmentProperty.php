<?php

namespace Archin\ExactOnline\Models;

use Picqer\Financials\Exact\ApiException;

class ItemAssortmentProperty extends \Picqer\Financials\Exact\ItemAssortmentProperty
{
    use Findable;

    /**
     * Get item assortment properties.
     *
     * @return array
     * @throws ApiException
     */
    public function getProperties()
    {
        return $this->getAll(
            'Code, Description, ItemAssortmentCode',
            sprintf(
                'ItemAssortmentCode eq %s or ItemAssortmentCode eq %s',
                Product::SUB_CATEGORY,
                Product::DETAIL_CATEGORY
            )
        );
    }
}
