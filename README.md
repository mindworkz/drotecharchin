# Koppeling Exact Online Drotech

This libary will set up a connection to Exact Online to retrieve product information of Drotech. Connecting to Exact
Online will be achieved via OAuth 2.0.

The library makes intensive use of the [Picqer's exact-php-client](https://github.com/picqer/exact-php-client).

## File permissions

Make sure the PHP process has write access to the `storage/` folder.

## Usage

Copy the file `.env.example` to `.env` and fill in the necessary configuration:

- ENCRYPTION_KEY: A random string used to encrypt/decrypt the secret keys
- EXACT_CLIENT_ID: Client ID of Exact Online App
- EXACT_CLIENT_SECRET: Client secret of Exact Online App
- REDIRECT_URL: URL to /callback.php

Navigate to /connect.php to initiate a connection. Log in with valid Exact Online credentials to grant this application
access.

Afterwards you're able to get 'test' data via /examples/*.php.

## Warning: security concerns

- Only expose the `public/` directory to the internet. NEVER EVER expose the `.env` in the root folder, or
  the `storage/` folder because they contain (encrypted) sensitive keys. Leaking these keys could lead to a major data breach.
- Make sure to protect the pages in the `public/` directory via, for example, basic HTTP authentication.
