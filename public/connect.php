<?php

use Archin\ExactOnline\ConnectionFactory;

error_reporting(E_ALL);
require '../vendor/autoload.php';

$connection = ConnectionFactory::create();
$connection->redirectForAuthorization();
