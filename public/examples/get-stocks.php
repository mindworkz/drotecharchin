<?php

use Archin\ExactOnline\ConnectionFactory;
use Archin\ExactOnline\Models\Product;

error_reporting(E_ALL);
require '../../vendor/autoload.php';

$connection = ConnectionFactory::create();
$connection->connect();

$product = new Product($connection);
$stocks = $product->getStocks();

var_dump($stocks);
