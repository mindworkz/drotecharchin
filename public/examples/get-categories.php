<?php

use Archin\ExactOnline\ConnectionFactory;
use Archin\ExactOnline\Models\Category;

error_reporting(E_ALL);
require '../../vendor/autoload.php';

$connection = ConnectionFactory::create();
$connection->connect();

$categories = new Category($connection);
$categories = $categories->getCategories();

var_dump($categories);
