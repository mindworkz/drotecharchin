<?php

use Archin\ExactOnline\ConnectionFactory;
use Archin\ExactOnline\Models\SalesPrices;

error_reporting(E_ALL);
require '../../vendor/autoload.php';

$connection = ConnectionFactory::create();
$connection->connect();

$salesPrices = new SalesPrices($connection);
$salesPrices = $salesPrices->getSalesPrices();

var_dump($salesPrices);
