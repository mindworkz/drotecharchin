<?php

use Archin\ExactOnline\ConnectionFactory;
use Archin\ExactOnline\Models\Customer;

error_reporting(E_ALL);
require '../../vendor/autoload.php';

$connection = ConnectionFactory::create();
$connection->connect();

$customer = new Customer($connection);
$customers = $customer->getCustomers();

var_dump($customers);
