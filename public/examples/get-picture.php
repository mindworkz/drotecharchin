<?php

use Archin\ExactOnline\ConnectionFactory;
use Archin\ExactOnline\Models\Picture;

error_reporting(E_ALL);
require '../../vendor/autoload.php';

$connection = ConnectionFactory::create();
$connection->connect();

$picture = new Picture($connection);

// Picture URL should be a valid URL to an image in Exact Online; use PictureUrl in array of a product
$pictureUrl = 'https://start.exactonline.nl/docs/SysImage.aspx?Table=Items&ID=7bb973df-7ac3-4b93-92a9-86e76f4c4fa0&ThumbSize=500&NoCache=1&OptimizeForWeb=1&_Division_=2371158';

header('Content-type: image/jpg');

exit($picture->download($pictureUrl));
