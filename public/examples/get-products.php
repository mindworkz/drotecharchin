<?php

use Archin\ExactOnline\ConnectionFactory;
use Archin\ExactOnline\Models\Product;

error_reporting(E_ALL);
require '../../vendor/autoload.php';

$connection = ConnectionFactory::create();
$connection->connect();

$product = new Product($connection);
$products = $product->getProducts();

$tree = [];

foreach ($products as $p) {
    // Item group
    if (!isset($tree[$p['ItemGroup']])) {
        $tree[$p['ItemGroup']] = [];
    }

    // Sub category
    if (!isset($tree[$p['ItemGroup']][$p['SubCategory']])) {
        $tree[$p['ItemGroup']][$p['SubCategory']] = [];
    }

    // Detail category
    if (!in_array($p['DetailCategory'], $tree[$p['ItemGroup']][$p['SubCategory']])) {
        $tree[$p['ItemGroup']][$p['SubCategory']][] = $p['DetailCategory'];
    }
}

var_dump($tree);
var_dump($products);
