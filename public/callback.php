<?php

use Archin\ExactOnline\ConnectionFactory;

error_reporting(E_ALL);
require '../vendor/autoload.php';

if (empty($_GET['code'])) {
    exit('Code is missing.');
}

$connection = ConnectionFactory::create();
$connection->setAuthorizationCode($_GET['code']);
$connection->connect();

echo 'Connection OK';
